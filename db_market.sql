-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 06-03-2022 a las 17:51:10
-- Versión del servidor: 5.7.31
-- Versión de PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_market`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) COLLATE utf8mb4_swedish_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_swedish_ci;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`category_id`, `category`, `status`) VALUES
(1, 'electronica', 1),
(2, 'Linea Blanca', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_swedish_ci DEFAULT NULL,
  `surname` varchar(45) COLLATE utf8mb4_swedish_ci DEFAULT NULL,
  `rfc` varchar(45) COLLATE utf8mb4_swedish_ci DEFAULT NULL,
  `mail` varchar(45) COLLATE utf8mb4_swedish_ci DEFAULT NULL,
  `region` int(11) DEFAULT NULL,
  PRIMARY KEY (`customer_id`),
  KEY `region` (`region`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_swedish_ci;

--
-- Volcado de datos para la tabla `customer`
--

INSERT INTO `customer` (`customer_id`, `name`, `surname`, `rfc`, `mail`, `region`) VALUES
(5, 'Anakin', 'Skywalker', 'SKT43GFVFD34', 'anakin@gmail.com', 4),
(6, 'Baby', 'Yoda', 'BY4GDSN34', 'yoda@gmail.com', 5),
(7, 'Obiwan', 'Kenobi', 'OKVVNS34GD', 'obiwi@gmail.com', 3),
(8, 'Dark', 'Vader', 'DAGNI3G8VNFD', 'darki@gmail.com', 4),
(9, 'Luis ', 'Rojas', 'ROEL543543GD', 'canitoescafe@gmail.com', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region`
--

DROP TABLE IF EXISTS `region`;
CREATE TABLE IF NOT EXISTS `region` (
  `region_id` int(11) NOT NULL AUTO_INCREMENT,
  `region` varchar(45) COLLATE utf8mb4_swedish_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`region_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_swedish_ci;

--
-- Volcado de datos para la tabla `region`
--

INSERT INTO `region` (`region_id`, `region`, `status`) VALUES
(3, 'Nezahualcoyolt', 0),
(4, 'Chimalhuacan', 0),
(5, 'Chalco', 0);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`region`) REFERENCES `region` (`region_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
