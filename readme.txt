Luis Angel Rojas Espinoza

Instrucciones de ejecución:

- Cargar customer a spring tools
- Ejecutar un servidor de apache, en este caso usando wampserver
- Cargar la base de datos en el manejador de nuestra preferencia, en este caso hicimos uso de MySQL workbench
- Posteriomente ejecutaremos el programa 
- Para realizar las operaciones usaremos postman para probar nuestra api. Probar operaciones con:

	GET http://localhost:8081/category
	GET http://localhost:8081/category/2
	POST http://localhost:8081/category
	body: 
{
    "category": "Linea Blanca",
    "status": "1"
}	
	PUT http://localhost:8081/category/5
{
    "category": "Linea blanca"
}
	 DELETE http://localhost:8081/category/5

Observaciones: 
Esta practica es la misma que la 3, esto debido a que en la practica 3 tuve una conficución y decidi implementar category para poder 
realizar la operaciones en base de datos debido a que pregunte y el profesor ya nos habia permitido hacer uso de base de datos desde la pracrtica 3